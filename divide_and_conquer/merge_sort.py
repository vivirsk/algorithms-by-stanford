#!/bin/python3
"""
My implementation of the MergeSort algorithm.
This is for algorithm learning purposes only.
"""


def merge(left_half, right_half):
    """
    Merge 2 sorted lists.

    :param left_half:       sorted list C
    :param right_half:      sorted list D
    :return:                sorted list B
    """
    i = 0
    j = 0

    B = []

    while i < len(left_half) and j < len(right_half):

        if left_half[i] <= right_half[j]:
            B.append(left_half[i])
            i += 1
        else:
            B.append(right_half[j])
            j += 1

    B += left_half[i:]
    B += right_half[j:]

    print("result: ", B)

    return B


def mergeSort(A):
    """
    Input:      list A of n distinct integers.
    Output:     list with the same integers, sorted from smallest to largest.

    :return:    Output
    """

    # base case
    if len(A) < 2:
        return A

    # divide the list into two
    mid = len(A) // 2
    print(mid)

    left = A[:mid]      # recursively sort first half of A
    right = A[mid:]     # recursively sort second half of A

    x = mergeSort(left)
    y = mergeSort(right)
    return merge(x, y)


print(mergeSort([1, 3, 2, 4, 6, 5]))
