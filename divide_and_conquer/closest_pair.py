#!/bin/python3

__author__ = "Vivien Ruska"
"""
My implementation of the Closest Pair problem in Python3.
This is for algorithm learning purposes only.
"""
import math


def merge(left, right, key=None):

    i = 0
    j = 0

    result = []

    while i < len(left) and j < len(right):

        if key(left[i]) <= key(right[j]):
            print(key(left[i]), key(right[j]))
            result.append(left[i])
            i += 1
        else:
            result.append(right[j])
            j += 1

    result += left[i:]
    result += right[j:]

    return result


def mergeSort(A, key=None):

    if len(A) < 2:
        return A

    middle = len(A) // 2
    left_half = A[:middle]
    right_half = A[middle:]

    left_sorted = mergeSort(left_half, key)
    right_sorted = mergeSort(right_half, key)
    merged = merge(left_sorted, right_sorted, key)

    return merged


def ClosestSplitPair(Px, Py, gamma):
    """
    running time:   linear --> O(n)
    Identifies the closest split pair only when it is the closest pair
    overall.

    :param Px:      sorted by x-coordinate
    :param Py:      sorted by y-coordinate
    :param gamma:   distance gamma between the closest pair that is a left or
                    right pair

    The subroutine then knows that it has to worry only about split pairs with
    interpoint distance less than gamma.

    :return:
    """

    # largest x-coordinate in left half
    x = Px[:(len(Px) // 2)][-1][0]  # median x-coordinate --> O(1)

    best = gamma
    best_pair = None

    # filtering step:   linear time (O(n))
    # scanning through Py and removing any points with an x-coordinate
    # outside the range of interest

    Sy = []

    for point in Py:
        if point[0] < (x - gamma) or point[0] > (x + gamma):
            continue
        else:
            # points q1, q2, ..., ql with x-coordinate between x-gamma and
            # x+gamma, sorted by y-coordinate
            Sy.append(point)

    l = len(Sy)

    # performs brute-force search over the pairs of points of Sy that have at
    # most 6 points in between them and computes the closest such pair of
    # points
    for i in range(l-1):
        for j in range(l - 1, i, -1):
            if get_distance(Sy[i], Sy[j]) < best:
                best = get_distance(Sy[i], Sy[j])
                best_pair = [Sy[i], Sy[j]]

    return best_pair


def get_distance(a, b):
    """
    Gets Euclidean distance between two points a, b.
    """
    delta = (a[0] - b[0]) ** 2 + (a[1] - b[1]) ** 2
    print(a, b, math.sqrt(delta))
    if delta > 0:
        return math.sqrt(delta)
    else:
        return 0


def ClosestPair(Px, Py):
    """
    running time:   O(n log n)

    :param Px:      sorted by x-coordinate
    :param Py:      sorted by y-coordinate
    :return:        the pair pi, pj of distinct points with smallest Euclidean
                    distance between them

    #TODO: handle the case of multiple pair being the closest.
    """

    # base case of <= 3 points omitted
    if len(Px) <= 3:
        return Px

    middle = len(Px) // 2
    first_half_of_Px = Px[:middle]
    second_half_of_Px = Px[middle:]

    # first half of Px, sorted by x-coordinate
    Lx = mergeSort(first_half_of_Px, key=lambda x: x[0])
    # first half of Px, sorted by y-coordinate
    Ly = mergeSort(first_half_of_Px, key=lambda y: y[1])
    # second half of Px, sorted by x-coordinate
    Rx = mergeSort(second_half_of_Px, key=lambda x: x[0])
    # second half of Px, sorted by y-coordinate
    Ry = mergeSort(second_half_of_Px, key=lambda y: y[1])

    (l1, l2) = ClosestPair(Lx, Ly)  # best left pair
    (r1, r2) = ClosestPair(Rx, Ry)  # best right pair

    min_distance = 1000
    best_pair = None

    for pair in [(l1, l2), (r1, r2)]:
        distance = get_distance(*pair)
        min_distance = min(min_distance, distance)
        if distance == min_distance:
            best_pair = pair

    split_pair = ClosestSplitPair(Px, Py, min_distance)  # best split pair

    if split_pair:
        if get_distance(*split_pair) < min_distance:
            best_pair = split_pair

    return best_pair


if __name__ == '__main__':
    test_case = [
        (1, 5), (2, 6), (3, 8), (4, 2),
        (5, 3), (6, 7), (7, 1), (9, 4)]
    test_case2 = [
        (7, 1), (4, 2), (5, 3), (9, 4),
        (1, 5), (2, 6), (6, 7), (3, 8)]

    print(ClosestPair(test_case, test_case2))
